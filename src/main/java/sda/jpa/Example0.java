package sda.jpa;

import sda.jpa.model.Klient;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Example0 {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("sda.jpa.1");

        EntityManager em = emf.createEntityManager();

        // TODO: wyswietlic klienta o ID=1
        Klient klient = em.find(Klient.class, 1L);

        System.out.println("Imie: " + klient.getImie());

        em.close();

        emf.close();
    }
}
