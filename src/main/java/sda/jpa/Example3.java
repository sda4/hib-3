package sda.jpa;

import sda.jpa.model.Klient;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

public class Example3 {

    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("sda.jpa.1");

        EntityManager em = emf.createEntityManager();

        // TODO: wczytac klienta o dowolnym ID i uaktualnic jego imie i nazwisko
        EntityTransaction et = em.getTransaction();
        et.begin();

        Klient klient = em.find(Klient.class, 15L);
        klient.setImie("Szymon Piotr");
        klient.setNazwisko("Rybakowski");

        et.commit();

        em.close();
        emf.close();
    }
}
