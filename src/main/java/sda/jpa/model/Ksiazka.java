package sda.jpa.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Ksiazka {
    @Id
    @Column(name = "ksiazka_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idKsiazki;

    private String tytul;

    @Column(name = "autor_imie")
    private String imieAutora;

    @Column(name = "autor_nazwisko")
    private String nazwiskoAutora;

    @Column(name="min_wiek")
    private Integer wiekMinimalnyCzytelnika;

    @Column(name = "rodzaj")
    private String rodzajKsiazki;
}
