package sda.jpa.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "ocena")
public class OcenaKsiazki {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ocena_id")
    private Long idOceny;

    @OneToOne
    @JoinColumn(name="ksiazka_id", referencedColumnName = "ksiazka_id")
    private Ksiazka ocenianaKsiazka;

    @OneToOne
    @JoinColumn(name="klient_id", referencedColumnName = "klient_id")
    private Klient oceniajacy;

    private Integer ocena;
}
