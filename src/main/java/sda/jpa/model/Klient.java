package sda.jpa.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Data
@Table(name="klient")
public class Klient {

    @Id
    @Column(name = "klient_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long klientId;

    private String imie;

    private String nazwisko;

    @Transient
    private Long licznikKsiazek;
}
