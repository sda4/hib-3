package sda.jpa;

import sda.jpa.model.Klient;
import sda.jpa.model.Ksiazka;
import sda.jpa.model.OcenaKsiazki;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Example4 {
    public static void main(String[] args) {


        EntityManagerFactory emf = Persistence.createEntityManagerFactory("sda.jpa.1");

        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();;

        // TODO: utworzyc nowego klienta
        Klient klient = new Klient();
        klient.setImie("Pawel");
        klient.setNazwisko("Duda");
        em.persist(klient);

        // TODO: utworzyc nowa ksiazke
        Ksiazka ksiazka = new Ksiazka();
        ksiazka.setTytul("W pustyni i w puszczy");
        ksiazka.setImieAutora("Henryk");
        ksiazka.setNazwiskoAutora("Sienkiewicz");
        ksiazka.setWiekMinimalnyCzytelnika(10);
        ksiazka.setRodzajKsiazki("SF");
        em.persist(ksiazka);

        // TODO: utworzyc ocene ksiazki
        OcenaKsiazki ocena = new OcenaKsiazki();
        ocena.setOcena(10);
        ocena.setOceniajacy(klient);
        ocena.setOcenianaKsiazka(ksiazka);
        em.persist(ocena);

        transaction.commit();
        em.close();
        emf.close();
    }
}
