package sda.jpa;

import sda.jpa.model.Klient;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

public class Example2 {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("sda.jpa.1");

        EntityManager em = emf.createEntityManager();

        // TODO: wyswietlic wszystkich klientow (o imieniu Jacek i nazwisku Szymanski) z uzyciem criteria API

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Klient> q = cb.createQuery(Klient.class);
        Root<Klient> c = q.from(Klient.class);

        ParameterExpression<String> parametrImie = cb.parameter(String.class);
        ParameterExpression<String> parametrNazwisko = cb.parameter(String.class);

        q.select(c)
                .where(cb.and(
                        cb.equal(c.get("imie"), parametrImie),
                        cb.equal(c.get("nazwisko"), parametrNazwisko)));

        em.createQuery(q)
                .setParameter(parametrImie, "Szymon")
                .setParameter(parametrNazwisko, "Dembek")
                .getResultList()
                .forEach(klient -> System.out.println(klient.getImie()));

        em.find(Klient.class, 10L);
        em.close();

        emf.close();
    }
}
