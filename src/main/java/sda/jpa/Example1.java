package sda.jpa;

import sda.jpa.model.Klient;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class Example1 {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("sda.jpa.1");

        EntityManager em = emf.createEntityManager();

        // TODO: wyswietlicList wszystkich klientow (o imieniu Jacek) + (o nazwisku Szymanski)
        List<Klient> wynik = em.createQuery("select k from Klient k " +
                "where k.imie=:parametrImie and k.nazwisko=:parametrNazwisko")
                .setParameter("parametrImie", "Szymon")
                .setParameter("parametrNazwisko", "Dembek")
                .getResultList();

        wynik.forEach(klient -> System.out.println(klient.getImie().trim()));

        em.close();

        emf.close();
    }
}
